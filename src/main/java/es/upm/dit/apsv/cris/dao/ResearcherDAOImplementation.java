package es.upm.dit.apsv.cris.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Researcher;

public class ResearcherDAOImplementation implements ResearcherDAO {

	private static ResearcherDAOImplementation instance;

	private ResearcherDAOImplementation() {
	}

	public static ResearcherDAOImplementation getInstance() {
		if (null == instance) {
			instance = new ResearcherDAOImplementation();
		}
		return instance;
	}
	@Override
	public Researcher create(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
		   session.beginTransaction();
		   session.save(researcher);
		   session.getTransaction().commit();
		} catch (Exception e) {
		   // handle exceptions
		} finally {
		   session.close();
		}
		
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		Researcher researcher= null;
		Session session = SessionFactoryService.get().openSession();
		
		try {
		   session.beginTransaction();
		   researcher = session.get(Researcher.class, researcherId);
		   session.getTransaction().commit();
		} catch (Exception e) {
		   // handle exceptions
		} finally {
		   session.close();
		}
		
		return researcher;
	}

	@Override
	public Researcher update(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
		   session.beginTransaction();
		   session.saveOrUpdate(researcher);
		   session.getTransaction().commit();
		} catch (Exception e) {
		   // handle exceptions
		} finally {
		   session.close();
		}
		
		return researcher;
	}

	@Override
	public Researcher delete(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
		   session.beginTransaction();
		   session.delete(researcher);
		   session.getTransaction().commit();
		} catch (Exception e) {
		   // handle exceptions
		} finally {
		   session.close();
		}		
		return researcher;
	}

	public List<Researcher> readAll() {
		Session session = SessionFactoryService.get().openSession();
		List<Researcher> rs = null;
		try {
			session.beginTransaction();
			 rs = (List<Researcher>) (session.createQuery("from Researcher").list());
			session.getTransaction().commit();
		} catch (Exception e) {
		}finally {
			session.close();
		}
		return rs;
	}

	@Override
	public Researcher readByEmail(String email) {
		Session session = SessionFactoryService.get().openSession();
		Researcher r = null;
		try {
			session.beginTransaction();
			r = (Researcher) session
					.createQuery("select r from Researcher r where r.email= :email")
					.setParameter("email", email)
					.uniqueResult();
			session.getTransaction().commit();
		} catch ( Exception e ) {
		}	finally {		
			session.close();
		}
		return r;
	
	}
}
