package es.upm.dit.apsv.cris.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

public class PublicationDAOImplementation implements PublicationDAO{

	private static PublicationDAOImplementation instance;

	private PublicationDAOImplementation() {
	}

	public static PublicationDAOImplementation getInstance() {
		if (null == instance) {
			instance = new PublicationDAOImplementation();
		}
		return instance;
	}
	@Override
	public Publication create(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
		   session.beginTransaction();
		   session.save(publication);
		   session.getTransaction().commit();
		} catch (Exception e) {
		   // handle exceptions
		} finally {
		   session.close();
		}
		
		return publication;
	}

	@Override
	public Publication read(String publicationId) {
		Publication Publication= null;
		Session session = SessionFactoryService.get().openSession();
		
		try {
		   session.beginTransaction();
		   Publication = session.get(Publication.class, publicationId);
		   session.getTransaction().commit();
		} catch (Exception e) {
		   // handle exceptions
		} finally {
		   session.close();
		}
		
		return Publication;
	}

	@Override
	public Publication update(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		try {
		   session.beginTransaction();
		   session.saveOrUpdate(publication);
		   session.getTransaction().commit();
		} catch (Exception e) {
		   // handle exceptions
		} finally {
		   session.close();
		}
		
		return publication;
	}

	@Override
	public Publication delete(Publication publication) {
		Session session = SessionFactoryService.get().openSession();
		
		try {
		   session.beginTransaction();
		   session.delete(publication);
		   session.getTransaction().commit();
		} catch (Exception e) {
		   // handle exceptions
		} finally {
		   session.close();
		}	
		return publication;
		
	}

	public List<Publication> readAll() {
		List<Publication> PublicationsList = new ArrayList<>();
		Session session = SessionFactoryService.get().openSession();
		
		try {
		   session.beginTransaction();
		   PublicationsList = session.createQuery("from Publication").list();
		   session.getTransaction().commit();
		} catch (Exception e) {
		   // handle exceptions
		} finally {
		   session.close();
		}
		
		return PublicationsList;
	}

	@Override
	public List<Publication> readAllPublications(String researcherId) {
		
	  	List<Publication> ps = new ArrayList<Publication>();
		for (Publication p: this.readAll())
			if(p.getAuthors().indexOf(researcherId) > -1) ps.add(p);
	  	return ps;
		
	}
	 
	/*		Session session = SessionFactoryService.get().openSession();
	List<Publication> ps = new ArrayList<Publication>();
	for (Publication p: this.readAll())
	try {
		session.beginTransaction();
		ps = (List<Publication>) session
				.createQuery("select p from Publication p where p.researcherId= :researcherId")
				.setParameter("researcherId", researcherId);
		session.getTransaction().commit();
	} catch ( Exception e ) {
	}	finally {		
		session.close();
	}
	return ps;
}*/

}
