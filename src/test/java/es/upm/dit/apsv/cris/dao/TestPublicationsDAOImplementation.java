package es.upm.dit.apsv.cris.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Test;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

public class TestPublicationsDAOImplementation {
	
	/**
	 * Comprueba que al crearse varias Ps, si se accede con readAll() estos Ps son los mismo que 
	 * los objeto P iniciales.
	 * 
	 */
	@Test
	public void testCreatePublicationCreatedMatchsPublicationRead() {
		
		Publication p1 = new Publication();
		p1.setId("1234567");
		PublicationDAO pdao = PublicationDAOImplementation.getInstance();
		pdao.create(p1);
		
		Publication pread = pdao.read(p1.getId());
		
		assertEquals(pread.getId(), p1.getId());
	
		pdao.delete(p1);
	}
	
	/**
	 * Comprueba que al crearse varios Ps, si se accede con readAll() estos Ps son los mismo que 
	 * los objeto P iniciales.
	 * 
	 */
	@Test
	public void testReadAll() {	
		
		Publication p1 = new Publication();
		p1.setId("123456");
		Publication p2 = new Publication();
		p2.setId("78912345");
		
		PublicationDAO pdao = PublicationDAOImplementation.getInstance();
		
		assertFalse(pdao.readAll().contains(p1), "There is no initial p1");
		assertFalse(pdao.readAll().contains(p2), "There is no initial p2");
		
		pdao.create(p1);
		pdao.create(p2);
		
		for(Publication p : pdao.readAll()) {
			System.out.println(p.getId());
			if(p.getId() == p1.getId()) {
				assertTrue(p.getId() == p1.getId(), "There is p1");
			}
			if(p.getId() == p2.getId()) {
				assertTrue(p.getId() == p2.getId(), "There is p1");
				}
			}	
		
		pdao.delete(p1);
		pdao.delete(p2);
		}
	
	/**
	 * Comprueba que si se crea una P asociado a un researcher readAllPublication muestra publications entre las que se encuentran esta.
	 * 
	 * 1. Se crea un researcher y un P distintos ascoiados a él y se guardan.
	 * 2. Then  it is done readAllPublications by id y se comprueba que entre las publications está la que se ha creado (checking id)
	 */
	@Test
	public void testReadAllPublications() {		
		
		// 1.
		Researcher r;
		ResearcherDAO rdao; 
	    rdao = ResearcherDAOImplementation.getInstance();
	    r = new Researcher();
	    r.setEmail("12345343100");
	    r.setId("785947432097");
	    r.setLastName("Zheng");
	    r.setName("LingFeng");
	    r.setPassword("1234");
	    rdao.create(r);
	       
		Publication p1 = new Publication();
		p1.setId("123456");
		p1.setAuthors(r.getId());
		
		PublicationDAO pdao = PublicationDAOImplementation.getInstance();
		
		assertFalse(pdao.readAll().contains(p1), "There is no initial p1");
		
		pdao.create(p1);

		
		// 2.
		
		for(Publication p : pdao.readAllPublications("785947432097")) {
			System.out.println(p.getId());
			if(p.getId() == p1.getId()) {
				assertTrue(p.getId() == p1.getId(), "There is p1");
			}
			}	
		
		pdao.delete(p1);
		}

	
}
