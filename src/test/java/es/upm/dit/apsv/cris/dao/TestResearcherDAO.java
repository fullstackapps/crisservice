package es.upm.dit.apsv.cris.dao;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.upm.dit.apsv.cris.dao.ResearcherDAO;
import es.upm.dit.apsv.cris.dao.ResearcherDAOImplementation;
import es.upm.dit.apsv.cris.dataset.CSV2DB;
import es.upm.dit.apsv.cris.model.Researcher;

class TestResearcherDAO {

	  private Researcher r;
	  private ResearcherDAO rdao;   
	  
	  @BeforeAll
	  static void dbSetUp() throws Exception {
	        CSV2DB.loadPublicationsFromCSV("publications.csv");
	        CSV2DB.loadResearchersFromCSV("researchers.csv");
	  }
	   
	   @BeforeEach
	   void setUp() throws Exception {
	         rdao = ResearcherDAOImplementation.getInstance();
	         r = new Researcher();
	         r.setEmail("12345343100");
	         r.setId("12345343100");
	         r.setLastName("Ferreiros");
	         r.setName("Javier");
	         r.setPassword("1234");
	         r.setScopusURL("https://www.scopus.com/authid/detail.uri?authorId=12345343100");
	   }
	   
	   
	   
		@Test
		static void test() throws Exception {		
			fail("Not yet implemented");
		}

}
