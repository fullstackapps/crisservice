Authors: Adrian Blazquez Leon / Jaafar Belouaret

This project consists: first in defining a class model, turning into a JPA entity and defining the Data Access Object (DAO) using the facade pattern (both interface and implementation) to store and retrieve objects from a relational database (H2). 
Then we will handle our persistence entities as resources and offer access by HTTP as RESTful services that use JSON for data transfer. JAX-RS are the standard Java APIs for all these operations.

JPA

- [x] Model classes => Publication and Researcher
- [x] Turning classes into JPA entities.
- [x] Data Access Object interface
- [x] DAO implementations
- [x] Access to DAO implementations
- [x] Testing

REST

- [x] REST services as a web application
- [x] Entities as REST resources
- [x] Implementing REST service methods
- [x] Synchronous access to external Scopus API-REST
- [ ] Asynchronous access to services through Google PubSub
- [x] Testing

![Service running](https://gitlab.com/fullstackapps/crisservice/-/raw/master/Screenshot%20from%202020-11-19%2020-09-59.png)

